#include <iostream>
#include <mpi.h>
//#include "/usr/local/include/mpi.h"
#define MCW MPI_COMM_WORLD

using namespace std;

int ring(const char *m, int icf, int data){
    // TODO: write your code here
    /* ignore m */
    int size, rank;
    MPI_Comm_size(MCW, &size); 
    MPI_Comm_rank(MCW, &rank); 
    if (icf != -1 && icf != 1){
        if (rank == 0) {
            cout << "invalid icf. aborting." << endl;
        }
        return data;
    }
    if (icf == 1) {
        MPI_Send(&rank, 1, MPI_INT, (rank + 1) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == -1) {
        MPI_Send(&rank, 1, MPI_INT, (rank + size - 1) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    
    return data;
}

void printRunning(const char* m, int running[64]){
    int size;
    MPI_Comm_size(MCW, &size); 
    cout<<m<<": ";
    for(int i=0;i<size;++i)cout<<running[i]<<" ";
    cout<<endl;
}
using namespace std;

int main(int argc, char **argv){

    int rank, size;
    int data;
    int running[64];
    const char *m="11111111";
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 

    data = ring(m,1,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);

    MPI_Barrier(MCW);

    data = ring(m,-1,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);

    MPI_Barrier(MCW);

    data = ring(m,-4,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);

    MPI_Barrier(MCW);

    MPI_Finalize();

    return 0;
}


