#include <iostream>
#include <iomanip>
#include <mpi.h>
#include <cmath>
//#include "/usr/local/include/mpi.h"
#define MCW MPI_COMM_WORLD

using namespace std;

enum illiacMeshICF {plus1,minus1,plusn,minusn,up,down,lft,rght};
enum pm2iICF {pls,mins};

int mask(const char *m){
    /* m[7] corresponds to 0th bit */
    // return 1 if you match the mask
    // return 0 if you don't
    int rank, r, size;
    bool match=true;
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 
    r=rank;
    for(int bit=7;bit>=0;--bit){
        if(m[bit]!='X'){
            if(r%2==0&&m[bit]=='1')match=false;
            if(r%2==1&&m[bit]=='0')match=false;
        }
        r/=2;
    }
    if(match){
        return 1;
    }else{
        return 0;
    }
}
 int pm2i(const char *m, enum pm2iICF pORm, int i, int data){
    if (mask(m)==0) return data;
    int rank, size;    
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 

    int change = pow(2,i);
    int direction = 1;

    if(pORm==mins) direction = -1;

    int destinationRank = (rank + (change * direction) + size) % size;

    MPI_Send(&data,1,MPI_INT,destinationRank,0,MCW);
    MPI_Recv(&data,1,MPI_INT,MPI_ANY_SOURCE,0,MCW,MPI_STATUS_IGNORE);
    
    return data;

}

int illiacMesh(const char *m, illiacMeshICF icf, int data){
    int rank,size,dest;
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 
    int n;
    n=sqrt(size);
    if(n*n!=size){
        cout<<"error: number of processes is not an even power of 2. ";
        cout<<"Quitting. ";
        exit(1);
    }
    if (icf == plus1) {
        data = pm2i(m, pls, 0, data);
    } 
    if (icf == minus1) {
        data = pm2i(m, mins, 0, data);
    } 
    if (icf == plusn) {
        data = pm2i(m, pls, n, data);
    } 
    if (icf == minusn) {
        data = pm2i(m, mins, n, data);
    } 
    return data;
}
int illiacMesh_old(const char *m, illiacMeshICF icf, int data){
    int rank,size,dest;
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 
    int n;
    n=sqrt(size);
    if(n*n!=size){
        cout<<"error: number of processes is not an even power of 2. ";
        cout<<"Quitting. ";
        exit(1);
    }

    //TODO: write your code here
    // {plus1,minus1,plusn,minusn,up,down,lft,rght};
    if (icf == plus1){
        MPI_Send(&rank, 1, MPI_INT, (rank + 1) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == minus1){
        MPI_Send(&rank, 1, MPI_INT, (rank + size - 1) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == plusn){
        MPI_Send(&rank, 1, MPI_INT, (rank + n) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == minusn){
        MPI_Send(&rank, 1, MPI_INT, (rank + size - n) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == up){
        MPI_Send(&rank, 1, MPI_INT, (rank + size - n) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == down){
        MPI_Send(&rank, 1, MPI_INT, (rank + n) % size, 0, MCW);
        MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
    }
    if (icf == lft){
        int mod = rank / n;
        mod = (mod * n)-1;
        mod = mod + 1;
        //mod = mod + 1;
        //cout << "rank " << rank << " mod " << mod << " rank - 1 % mod " << (rank - 1) % mod << endl;
        if (rank - 1 == -1 || rank == mod){
            int jump = rank / n;
            jump = jump + 1;
            jump = (jump * n)-1;
            MPI_Send(&rank, 1, MPI_INT, jump, 0, MCW);
            MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        
        }
        else {
            MPI_Send(&rank, 1, MPI_INT, rank - 1, 0, MCW);
            MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        }
    }
    if (icf == rght){
        int mod = rank / n;
        mod = mod + 1;
        mod = (mod * n)-1;
        mod = mod + 1;
        //cout << "rank " << rank << " mod " << mod << endl;
        //cout << "rank " << rank << " sending to " << (rank + 1) % mod << endl;
        if ((rank + 1) % mod == 0){
            int jump = rank / n;
            jump = (jump * n)-1;
            jump = jump + 1;
            MPI_Send(&rank, 1, MPI_INT, jump, 0, MCW);
            MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        
        }
        else {
            MPI_Send(&rank, 1, MPI_INT, rank + 1, 0, MCW);
            MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, 0, MCW, MPI_STATUS_IGNORE);
        }
    }
     
    return data;
}

void printRunning(const char* m, int running[64]){
    int rank,size;
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 
    int n;
    n=sqrt(size);
    //cout<<m<<": ";
    for(int i=0;i<n;++i){
        for(int j=0;j<n;++j){
            cout<<setw(3)<<running[i*n+j]<<" ";
        }
        cout<<endl;
    }
}
using namespace std;

int main(int argc, char **argv){

    int rank, size;
    int data;
    int running[64];
    const char *m="XXXXXXXX";
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 

    if(!rank) cout<<"\nplus 1:\n";
    data = illiacMesh(m,plus1,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nminus 1:\n";
    data = illiacMesh(m,minus1,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nplus n:\n";
    data = illiacMesh(m,plusn,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nminus n:\n";
    data = illiacMesh(m,minusn,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nup:\n";
    data = illiacMesh(m,up,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\ndown:\n";
    data = illiacMesh(m,down,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nleft:\n";
    data = illiacMesh(m,lft,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    if(!rank) cout<<"\nright:\n";
    data = illiacMesh(m,rght,rank);
    MPI_Gather(&data,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m,running);
    MPI_Barrier(MCW);

    MPI_Finalize();

    return 0;
}


