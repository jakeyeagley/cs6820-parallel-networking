
#include <iostream>
#include <mpi.h>
//#include "/usr/local/include/mpi.h"
#define MCW MPI_COMM_WORLD

using namespace std;

int mask(const char *m){
    /* m[7] corresponds to 0th bit */
    // TODO: write your code here
    
    int rank;
    MPI_Comm_rank(MCW, &rank); 
    if (m[5] == 'X' && m[6] == 'X' && m[7] == '1') {
        if (rank % 2 == 1) {
            return 1;
        }
    }
    else if (m[5] == 'X' && m[6] == 'X' && m[7] == '0') {
        if (rank % 2 == 0) {
            return 1;
        }
    }
    else if (m[5] == 'X' && m[6] == '0' && m[7] == '0') {
        if (rank % 4 == 0) {
            return 1;
        }
    }
    else if (m[5] == '1' && m[6] == 'X' && m[7] == '1') {
        if (rank % 8 == 5 || rank % 8 == 7) {
            return 1;
        }
    }
    else if (m[5] == '1' && m[6] == '1' && m[7] == '0') {
        if (rank % 8 == 6){
            return 1;
        }  
    }
    else if (m[7] == 'X' && m[6] == 'X' && m[5] == 'X' ) {
        return 1;
    }
    return 0;

    // return 1 if you match the mask
    // return 0 if you don't

    /* incorrect code follows */
}

void printRunning(const char* m, int running[64]){
    int size;
    MPI_Comm_size(MCW, &size); 
    cout<<m<<": ";
    for(int i=size-1;i>=0;--i)cout<<running[i];
    cout<<endl;
}
    
int main(int argc, char **argv){
    int running[64];

    int rank, size;
    int localrunning;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 

    const char *m1 = "XXXXXXXX";
    localrunning = mask(m1);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m1,running);

    const char *m2 = "XXXXXXX0";
    localrunning = mask(m2);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m2,running);

    const char *m3 = "XXXXXXX1";
    localrunning = mask(m3);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m3,running);

    const char *m4 = "XXXXXX00";
    localrunning = mask(m4);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m4,running);

    const char *m5 = "XXXXX1X1";
    localrunning = mask(m5);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m5,running);

    const char *m6 = "XXXXX110";
    localrunning = mask(m6);
    MPI_Gather(&localrunning,1,MPI_INT,running,1,MPI_INT,0,MCW);
    if(rank==0)printRunning(m6,running);

    MPI_Finalize();

    return 0;
}

